import axios from 'axios';

let baseURL = process.env.baseURL || "https://djangelsapp.com/djangelsapp/api";

let instance = axios.create({
	baseURL
});

export const HTTP = instance;